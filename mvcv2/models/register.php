<?php


class Register
{
    private $last_name;
    private $first_name;
    private $date;
    private $mail;
    private $password;
    private $cpassword;
    private $type;

    public function __construct($last_name, $first_name, $date, $mail, $password, $cpassword, $type)
    {
        $this->last_name = $last_name;
        $this->first_name = $first_name;
        $this->date = $date;
        $this->mail = $mail;
        $this->password = $password;
        $this->cpassword = $cpassword;
        $this->type = $type;
    }
    public function verifyPasswords()
    {
        if ($this->password != $this->cpassword)
            return "false";
        else
            return "true";
    }

    public function existentUniqueToken()
    {
        require '../dbh.inc.php';
        $sql = 'select * from tokensforregister where token = ?';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $this->type);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        if ($rez->num_rows === 1)
            return "true";
        else
            return "false";
    }

    public function getTypeOfUniqueToken($existentUniqueToken)
    {
        require '../dbh.inc.php';
        $sql = 'select type from tokensforregister where token = ?';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $this->type);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        $inregistrare = $rez->fetch_assoc();
        return $inregistrare['type'];
    }

    public function existentEmail($TypeOfUniqueToken)
    {
        require '../dbh.inc.php';
        if ($TypeOfUniqueToken == 'student') {
            $sql = 'select * from students where email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            if ($rez->num_rows === 1)
                return "true";
            else
                return "false";
        } else  if ($TypeOfUniqueToken == 'teacher') {
            $sql = 'select * from teachers where email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            if ($rez->num_rows === 1)
                return "true";
            else
                return "false";
        }
    }




    public function register($samePasswords, $existentEmail,$TypeOfUniqueToken)
    {
        require '../dbh.inc.php';
        if ($samePasswords == "true" && $existentEmail == "false") {
            if ($TypeOfUniqueToken == 'student') {
                $date = date('Y-m-d H:i:s');
                $password_hashed= password_hash($this->password, PASSWORD_DEFAULT);
                $sql = 'insert into students (first_name,last_name,birthday,email,password,created_at,updated_at) values(?,?,?,?,?,?,?)';
                $rezultat = $conn->prepare($sql);
                $rezultat->bind_param('sssssss', $this->first_name, $this->last_name, $this->date, $this->mail, $password_hashed, $date, $date);
                $rezultat->execute();
                $rezultat->close();
                return "succes";
            } else
            if ($TypeOfUniqueToken == 'teacher') {
                $date = date('Y-m-d H:i:s');
                $password_hashed= password_hash($this->password, PASSWORD_DEFAULT);
                $sql = 'insert into teachers (first_name,last_name,birthday,email,password,created_at,updated_at) values(?,?,?,?,?,?,?)';
                $rezultat = $conn->prepare($sql);
                $rezultat->bind_param('sssssss', $this->first_name, $this->last_name, $this->date, $this->mail, $password_hashed, $date, $date);
                $rezultat->execute();
                $rezultat->close();
                return "succes";
            }
        }
    }
}
