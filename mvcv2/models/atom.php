<?php

class Atom 
{
    private $project_id;
    public function __construct($project_id)
    {
        $this->project_id = $project_id;
    }
    public function getAlerts()
    {
        require '../dbh.inc.php';
        $sql = 'select * from alerts where id_project = ? order by id desc';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('d', $this->project_id);
        $rezultat->execute();
        $rez = $rezultat->get_result();

        while($row = mysqli_fetch_array($rez)) {
            $rows[]=$row;
        }
        $rezultat->close();
        return $rows;
    }
   
}