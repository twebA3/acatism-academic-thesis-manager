<?php

class Task
{
    public $id;
    public $id_project;
    public $description;
    public $deadline;
    public $done;
    public $github;
    public $created_at;
    public $updated_at;

    public function add($id, $description,  $deadline, $github)
    {
        //trebuie implementata functia de add cu parametri corespunzatori
        //echo 'add task2';
        $this->addTask($id, $description,  $deadline, $github);
        $this->addAlert($id);
        $this->sendEmailAlert($id, $deadline, $description);
    }

    public function addTask($id, $description,  $deadline, $github)
    {
        require '../dbh.inc.php';
        $date = date('Y-m-d H:i:s');
        $sql = 'insert into tasks (id_project,description,deadline,github,created_at,updated_at) values(?,?,?,?,?,?)';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('ssssss', $id, $description,  $deadline, $github, $date, $date);
        $rezultat->execute();
        $rezultat->close();
        header("Location: ../controllers/viewproject-teacher.php?id=" . $id);
    }



    public function deleteTask($id,$id1)
    { 
        require '../dbh.inc.php';
    $sql = 'delete from tasks where id = ?';
    $rezultat = $conn->prepare($sql);
    $rezultat->bind_param('s', $id);
    $rezultat->execute();
    $rezultat->close();
    header("Location: ../controllers/viewproject-teacher.php?id=" . $id1);
    }

    public function displayTasks($id)
    {
        $projects = '';
        require '../dbh.inc.php';
        $sql = 'select * from tasks where id_project= ? order by id desc';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $id);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        while ($row = mysqli_fetch_array($rez)) {
            if ($row['done'] == 0)
                $projects .= '<div class="project">
                <form class="formstyle" action="../controllers/viewproject-teacher.php?id='.$id.'&id_task=' . $row['id'] . '" method="POST">
                <button name="delete" class="btn1"><i class="fa fa-trash"></i> Delete</button>
                </form>
                <h4 style="margin-left: 2%; color:#b20505;">Deadline: ' . $row['deadline'] . '</h5>
				<p style="text-align: justify; margin-left: 2%;">Description: ' . $row['description'] . '	
				</p>
      </div>';
            else
                $projects .= '<div class="project" style="background-color: #11998e;">
                <button name="done" class="btn1" disabled><i class="fa fa-check-square"></i> Done</button>
            
                
                <h4 style="color:#b20505;  margin-left: 2%;">Deadline: ' . $row['deadline'] . '</h4>
				<p style="text-align: justify; margin-left: 2%;">Description: ' . $row['description'] . '	
				</p>
      </div>';
        }
        return $projects;
    }

    private function addAlert($id_project)
    {
        $message = "New Task added to your project";
        require_once 'alert.php';
        Alert::add($id_project, $message);
    }
    private function addOverduelAlert($id_project, $deadline, $task_description)
    {
        $message = "You have an overdue task: '" . $task_description . "' deadline: " . $deadline;
        require_once 'alert.php';
        Alert::add($id_project, $message);
    }
    private function sendEmailAlert($id_project, $deadline, $description)
    {
        require_once 'student.php';
        $to      = Student::getEmailByProject($id_project);
        $subject = 'New task added to your project, deadline: ' . $deadline;
        $message = 'A new task was added to your project
        Task: ' . $description . '
        Deadline: ' . $deadline . '
        You can update your progress here: http://localhost/acatism-academic-thesis-manager/mvcv2/controllers/project?id=' . $id_project;
        require_once 'mail.php';
        Mail::send($to, $subject, $message);
    }
    private function sendEmailOverdueAlert($id_project, $deadline, $task_description)
    {
        require_once 'student.php';
        $to = Student::getEmailByProject($id_project);
        $subject = 'You have an overdue task, deadline: ' . $deadline;
        $message = 'You have an overdue task
        Task: ' . $task_description . '
        Deadline: ' . $deadline . '
        You can update your progress here: http://localhost/acatism-academic-thesis-manager/mvcv2/controllers/project?id=' . $id_project;
        require_once 'mail.php';
        Mail::send($to, $subject, $message);
        //teacher email:
        require_once 'teacher.php';
        $to = Teacher::getEmailByProject($id_project);
        $subject = 'One of your students have an overdue task, deadline: ' . $deadline;
        $message = 'One of your students have an overdue task:
        Task: ' . $task_description . '
        Deadline: ' . $deadline;
        Mail::send($to, $subject, $message);
    }
    public function markAsDone($id, $id_project)
    {
        require '../dbh.inc.php';
        $sql = 'UPDATE tasks SET done=1, updated_at=SYSDATE() WHERE id=? and id_project=?';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('dd', $id, $id_project);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $affected_rows = $conn->affected_rows;
        $rezultat->close();
        return ($affected_rows === 1);
    }
    public function getAllByProject($id_project)
    {
        require '../dbh.inc.php';
        $sql = "SELECT * FROM `tasks` where id_project=?";
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('d', $id_project);
        $rezultat->execute();
        $rez = $rezultat->get_result();

        $rows = [];
        while ($row = mysqli_fetch_array($rez)) {
            $rows[] = $row;
        }
        $rezultat->close();
        return $rows;
    }
    public function sendAlertsOverdue($id_project)
    {
        require '../dbh.inc.php';
        $sql = 'SELECT * FROM tasks WHERE done=false and deadline<sysdate() and id_project=? order by id desc';
        $rezultat = $conn->prepare($sql);

        $rezultat->bind_param('d', $id_project);
        $rezultat->execute();

        $rez = $rezultat->get_result();
        if ($rez->num_rows === 0)
            return;
        while ($row = mysqli_fetch_array($rez)) {
            $rows[] = $row;
        }
        $rezultat->close();
        foreach ($rows as $row) {
            self::addOverduelAlert($row['id_project'], $row['deadline'], $row['description']);
            self::sendEmailOverdueAlert($row['id_project'], $row['deadline'], $row['description']);
        }
    }
    public function getGithubTasks($id_project)
    {
        require '../dbh.inc.php';
        $sql = 'SELECT * FROM tasks WHERE github=1 and done=0';
        $rezultat = $conn->prepare($sql);
        $rezultat->execute();

        $rez = $rezultat->get_result();
        if ($rez->num_rows === 0)
            return [];
        while ($row = mysqli_fetch_array($rez)) {
            $rows[] = $row;
        }
        $rezultat->close();
        return $rows;
    }
}
