<?php
function updateProfile($types, $fields, $values, $table)
{
    require '../dbh.inc.php';

    $intrebari = '';
    $nrVal = sizeof($fields);
    for ($i = 0; $i < $nrVal; $i++) $intrebari = $intrebari . $fields[$i] . '=?,';
    $intrebari = $intrebari . 'updated_at=CURRENT_TIMESTAMP';

    $sql = "UPDATE " . $table . " SET " . $intrebari . " WHERE email=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param($types, ...$values);
    $stmt->execute();
    $stmt->close();
}
?>
