<?php

class Alert 
{
    public function add($id_project, $message)
    {
        require '../dbh.inc.php';
        $sql = "INSERT INTO `alerts`(`id_project`, `message`) VALUES (?,?)";
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('is', $id_project, $message);
        $rezultat->execute();
        $rezultat->close();
    }
}