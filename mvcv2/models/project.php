<?php

class Project 
{

    public function getProjecByEmail($email)
    {
        require '../dbh.inc.php';
        $sql = 'SELECT p.* from projects p inner join students s on s.id=p.id_student where s.email=?';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $email);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        if ($rez->num_rows === 0)
        {
            header("Location: all-proposals.php");
            return;
        }
        return $rez->fetch_assoc();
    }
    public function getAllInProgress()
    {
        require '../dbh.inc.php';
        $sql = 'SELECT * FROM projects WHERE done=0 and id_student IS NOT NULL';
        $rezultat = $conn->prepare($sql);
        $rezultat->execute();
        $rez = $rezultat->get_result();

        while($row = mysqli_fetch_array($rez)) {
            $rows[]=$row;
        }
        $rezultat->close();
        return $rows;
        
    }
    public function getAllProposals($theme = "")
    {
        require '../dbh.inc.php';
        $sql = "SELECT p.*,CONCAT(t.first_name, ' ', t.last_name) AS teacher_name FROM `projects` p inner join teachers t on p.id_teacher=t.id 
        WHERE id_student is NULL and lower(p.theme) LIKE ('%".$theme."%') and year=".date("Y");
        $rezultat = $conn->prepare($sql);
        $rezultat->execute();
        $rez = $rezultat->get_result();

        $rows = [];
        while($row = mysqli_fetch_array($rez)) {
            $rows[]=$row;
        }
        $rezultat->close();
       
        return $rows;
        
    }
    public function getAllInProgressWithGithub()
    {
        require '../dbh.inc.php';
        $sql = 'SELECT * FROM projects WHERE done=0 and id_student IS NOT NULL and github IS NOT NULL';
        $rezultat = $conn->prepare($sql);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rows = [];
        while($row = mysqli_fetch_array($rez)) {
            $rows[]=$row;
        }
        $rezultat->close();
        return $rows;
        
        
    }
    public function updateGithub($id, $github)
    {
        require '../dbh.inc.php';
        $sql = 'UPDATE projects SET github=? where id=?';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('ss', $github, $id);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $affected_rows = $conn->affected_rows;
        $rezultat->close();
        return ($affected_rows === 1);
    }
}