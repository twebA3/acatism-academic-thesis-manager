<?php

class Mail
{
    public function send($to, $subject, $message)
    {
        $headers = 'From: mihai@savedreplies.io' . "\r\n" .
        'Reply-To: mihai@savedreplies.io' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
    }
}