<?php
function validateHTML($continut)
{

    $errors = array();
    $warnings = array();
    $ignoredWarnings = array(
        "Tag main invalid", "Tag header invalid",
        "Tag aside invalid", "Tag article invalid", "Tag section invalid",
        "Tag footer invalid", "Tag nav invalid"
    );

    libxml_use_internal_errors(true);
    $doc = new DomDocument;
    $doc->loadHtml($continut);
    $eroriDOM = libxml_get_errors();

    foreach ($eroriDOM as $e) {
        $message = str_replace("\n", "", $e->message);
        switch ($e->level) {
            case 2:
                if (!in_array($message, $ignoredWarnings))
                    array_push($warnings, $message . " at line " . $e->line);
                break;
            default:
                array_push($errors, $message . " at line " . $e->line);
        }
    }

    $html = $doc->getElementsByTagName("html");
    if ($html->length != 1) {
        array_push($errors, "Document should have only one html tag");

        if ($html->length == 0)
            return json_encode(array("Errors" => $errors, "Warnings" => $warnings));
    }

    if (strcmp($html[0]->parentNode->nodeName, "#document"))
        array_push($errors, "html tag should not be closed within any other tag at line "
            . $html[0]->getLineNo());

    if (!$html[0]->hasAttribute("lang"))
        array_push($warnings, "html document should have lang attribute declared at line "
            . $html[0]->getLineNo());


    $bodyNode = null;
    $headNode = null;

    foreach ($html[0]->childNodes as $node) {
        switch ($node->nodeName) {
            case "body":
                if ($bodyNode == null) $bodyNode = $node;
                break;
            case "head":
                if ($headNode == null) $headNode = $node;
                break;
            case "#text":
                break;
            default:
                array_push($warnings, "The html tag should contain only head and body tags at line "
                    . $node->getLineNo());
        }
    }

    if ($headNode == null)
        array_push($errors, "No head tag inside html");
    else if ($headNode->getElementsByTagName("main")->length > 0)
        array_push($errors, "main tag should be a direct child of body");

    if ($bodyNode == null) {
        array_push($errors, "No body tag inside html");
        return json_encode(array("Errors" => $errors, "Warnings" => $warnings));
    }

    $main = $bodyNode->getElementsByTagName("main");
    if ($main->length != 1) {
        array_push($errors, "Document should have only one main tag");

        if ($main->length == 0)
            return json_encode(array("Errors" => $errors, "Warnings" => $warnings));
    }
    if (strcmp($main[0]->parentNode->nodeName, "body"))
        array_push($errors, "main tag should be a direct child of body at line "
            . $main[0]->getLineNo());

    

    // every header tag should have a h1-h6 

    $headers = $bodyNode->getElementsByTagName("header");
    foreach ($headers as $header) {
        $found = 0;
        foreach ($header->childNodes as $child) {
            if (preg_match("/(h[1-6])/", $child->nodeName) == 1) {
                $found = 1;
                break;
            }
        }
        if ($found == 0)
            array_push($warnings, "header tag should contain a h1-h6 tag at line " . $child->getLineNo());
    }

    $sections = $main[0]->getElementsByTagName("section");
    foreach ($sections as $section) {
        if (
            $section->firstChild == null ||
            preg_match("/(h[1-6])/", $section->firstChild->nodeName) != 1
        )
            array_push($warnings, "Every section must start with a heading at line " . $section->getLineNo());
    }

    return json_encode(array("Errors" => $errors, "Warnings" => $warnings));
}
?>