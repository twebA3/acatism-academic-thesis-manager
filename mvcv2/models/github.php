<?php

class Github
{
    function getCommits($username, $repo)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.github.com/repos/$username/$repo/commits",
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_FOLLOWLOCATION => true
        ]);

        $resp = curl_exec($curl);
        curl_close($curl);
        return json_decode($resp, true);
    }
    function getTaskId($message)
    {
        preg_match('#\[(\d+?)\]#', $message, $matches);        
        return isset($matches[1]) ? $matches[1] : 0;
        
    }
    function urlToUserAndRepo($url)
    {
        $params = parse_url($url, PHP_URL_PATH);
        $params = substr($params, 1);
        list($username, $repo) = explode('/', $params);
        $info = [
            'username' => $username,
            'repo' => $repo
        ];

        return $info;
    }   
    function checkForTasks($username, $repo, $project_id)
    {
        require_once '../models/task.php';
        require_once '../models/project.php';
        $response = self::getCommits($username, $repo);
       
        foreach($response as $resp)
        {
            $commit = $resp['commit'];
            $task_id = self::getTaskId($resp['commit']['message']);
            if($task_id === 0)
                continue;
            echo '<br>Task id:'.$task_id." marked as done\n";
            echo "$task_id proj: $project_id";
            Task::markAsDone($task_id, $project_id);
        }

    }
}