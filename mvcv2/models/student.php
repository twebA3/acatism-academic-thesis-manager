<?php

class Student
{
    public function getEmailByProject($id_project)
    {
        require '../dbh.inc.php';
        $sql = "select s.email from projects p inner join students s on s.id=p.id_student where p.id=?";
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $id_project);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        $email = $rez->fetch_assoc()['email'];
        return $email;
    }
}