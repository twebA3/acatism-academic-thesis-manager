<?php


class Create
{

  private $name;
  private $description;
  private $type;
  private $year;
  private $theme;


  public function __construct($name, $description, $type, $year, $theme)
  {
    $this->name = $name;
    $this->description = $description;
    $this->year = $year;
    $this->type = $type;
    $this->theme = $theme;
  }



  public function getID($mail)
  {
    require '../dbh.inc.php';
    $sql = 'select id from teachers where email = ?';
    $rezultat = $conn->prepare($sql);
    $rezultat->bind_param('s', $mail);
    $rezultat->execute();
    $rez = $rezultat->get_result();
    $rezultat->close();
    $inregistrare = $rez->fetch_assoc();
    return $inregistrare['id'];
  }

  public function create($id)
  {
    require '../dbh.inc.php';
    $date = date('Y-m-d H:i:s');
    $sql = 'insert into projects (id_teacher,name,description,type,theme,year,created_at,updated_at) values(?,?,?,?,?,?,?,?)';
    $rezultat = $conn->prepare($sql);
    $rezultat->bind_param('ssssssss', $id, $this->name, $this->description, $this->type,$this->theme, $this->year,$date,$date);
    $rezultat->execute();
    $rezultat->close();
  }

  public function displayProjects()
	{	$projects = '';
		require '../dbh.inc.php';
		$sql = "select * from projects ";
		$result = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_array($result))
		{
			$projects .= '<div class="project">
				<h4><a href="#">'.$row['name'].'</a></h4>
				<h5>Proiect '.$row['type'].', '.$row['created_at'].'</h5>
				<p>'.$row['description'].'	
				</p>
			</div>';
			
			
		}
		return $projects;
	}
}
