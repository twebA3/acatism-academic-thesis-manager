<?php

class Index 

{
	public function displayProjects()
	{	$projects = '';
		require '../dbh.inc.php';
		$sql = "select * from projects ";
		$result = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_array($result))
		{
			$projects .= '<div class="project">
				<h4><a href="#">'.$row['name'].'</a></h4>
				<h5>Proiect '.$row['type'].', '.$row['created_at'].'</h5>
				<p>'.$row['description'].'	
				</p>
			</div>';
			
			
		}
		return $projects;
	}

	public function Search($search)
	{	$projects = '';
		require '../dbh.inc.php';
		$sql = "select * from projects where lower(name) LIKE ('%".$search."%')";
		$result = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_array($result))
		{
			$projects .= '<div class="project">
				<h4><a href="#">'.$row['name'].'</a></h4>
				<h5>Proiect '.$row['type'].', '.$row['created_at'].'</h5>
				<p>'.$row['description'].'	
				</p>
			</div>';
			
			
		}
		return $projects;
	}

	

}
