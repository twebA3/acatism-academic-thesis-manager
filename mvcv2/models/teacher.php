<?php

class Teacher 
{
    public function getName($project_id)
    {
        require '../dbh.inc.php';
        $sql = "select t.first_name, t.last_name from projects p inner join teachers t on p.id_teacher=t.id where p.id = ?";
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $project_id);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        $teacher = $rez->fetch_assoc();
        return $teacher['first_name'].' '.$teacher['last_name'];
    }
    public function getEmailByProject($id_project)
    {
        require '../dbh.inc.php';
        $sql = "select t.email from projects p inner join teachers t on t.id=p.id_teacher where p.id=?";
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $id_project);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        $email = $rez->fetch_assoc()['email'];
        return $email;
    }
    public function getAll()
    {
        require '../dbh.inc.php';
        $sql = 'SELECT * FROM teachers';
        $rezultat = $conn->prepare($sql);
        $rezultat->execute();
        $rez = $rezultat->get_result();

        while($row = mysqli_fetch_array($rez)) {
            $rows[]=$row;
        }
        $rezultat->close();
        return $rows;
        
    }
}