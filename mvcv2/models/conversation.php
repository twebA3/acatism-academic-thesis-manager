<?php

function sendMessage($id_student, $id_teacher, $type_sender, $message) {
    require '../dbh.inc.php';

    $select_sql = "SELECT id FROM conversations WHERE student_id = ? AND teacher_id = ?";
    $insert_message_sql = "INSERT INTO messages(id_conv, content, type, created_at, updated_at) VALUES(?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";
    $insert_conversation_sql = "INSERT INTO conversations(student_id, teacher_id) VALUES(?,?)";

    $select_stmt = $conn->prepare($select_sql);
    $select_stmt->bind_param('ss', $id_student, $id_teacher);
    $select_stmt->execute();
    $rez = $select_stmt->get_result();
    $select_stmt->close();
    if ($rez->num_rows > 0) {
        $insert_mess_sql = $conn->prepare($insert_message_sql);
        $insert_mess_sql->bind_param('iss', $rez->fetch_row()[0], $message, $type_sender);
        $insert_mess_sql->execute();
        $insert_mess_sql->close();
    } else {
        $insert_conv_sql = $conn->prepare($insert_conversation_sql);
        $insert_conv_sql->bind_param('ii', $id_student, $id_teacher);
        $insert_conv_sql->execute();
        $idConv = mysqli_insert_id($conn);
        $insert_conv_sql->close();

        $insert_mess_sql = $conn->prepare($insert_message_sql);
        $insert_mess_sql->bind_param('iss', $idConv, $message, $type_sender);
        $insert_mess_sql->execute();
        $insert_mess_sql->close();
    }
}

function getField($field, $searchField, $searchValue, $type) {
    require '../dbh.inc.php';
    //echo "SELECT  . $field .  FROM  . $type .  WHERE  . $searchField = $searchValue ";
    $sql = "SELECT " . $field . " FROM " . $type . " WHERE " . $searchField . " = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $searchValue);
    $stmt->execute();
    $rez = $stmt->get_result();
    $stmt->close();
    if($rez->num_rows === 0) return -1;
    return $rez->fetch_row()[0];
}

function getConversations($myId, $type) {
    require '../dbh.inc.php';
    $sql = '';
    if($type == "student") {
        $sql = 'SELECT id, teacher_id FROM conversations WHERE student_id = ?';
    } else {
        $sql = 'SELECT id, student_id FROM conversations WHERE teacher_id = ?';
    }

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $myId);
    $stmt->execute();
    $rez = $stmt->get_result();


    $result = array();
    $partnerType = ($type == "student") ? "teachers" : "students";

    while($row = $rez->fetch_row()) {
        $result[getField("email", "id", $row[1], $partnerType)] = $row[0];
    }

    $stmt->close();

    return $result;
}

function getMessages($idConv) {
    require '../dbh.inc.php';
    $sql = 'SELECT content, type, created_at FROM messages WHERE id_conv = ? ORDER BY created_at asc';

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $idConv);
    $stmt->execute();
    $rez = $stmt->get_result();


    $result = array();

    while($row = $rez->fetch_row()) {
        array_push($result, array($row[0], $row[1], $row[2]));
    }

    $stmt->close();

    return $result;
}
?>
