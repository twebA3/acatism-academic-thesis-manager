<?php


class Login
{
    private $mail;
    private $pass;
    private $type;

    public function __construct($mail, $pass, $type)
    {
        $this->mail = $mail;
        $this->pass = $pass;
        $this->type = $type;
    }

    public function login($HashedPassword)
    {
        require '../dbh.inc.php';
        if ($this->type == 'student') {
            $check = password_verify($this->pass, $HashedPassword);
     
            if ($check == 1) {
                $sql = 'select * from students where email = ? and password = ?';
                $rezultat = $conn->prepare($sql);
                $rezultat->bind_param('ss', $this->mail, $HashedPassword);
                $rezultat->execute();
                $rez = $rezultat->get_result();
                $rezultat->close();
                if ($rez->num_rows === 1)
                    return "succes";
                
            }
            else
                    return "failed";
        } else if ($this->type == 'teacher') {
            $check = password_verify($this->pass, $HashedPassword);
            if ($check == 1) {
                $sql = 'select * from teachers where email = ? and password = ?';
                $rezultat = $conn->prepare($sql);
                $rezultat->bind_param('ss', $this->mail, $HashedPassword);
                $rezultat->execute();
                $rez = $rezultat->get_result();
                $rezultat->close();
                if ($rez->num_rows === 1)
                    return "succes";
             
            }
            else
            return "failed";
        }
    }

    public function getHashedPassword()
    {
        require '../dbh.inc.php';
        if ($this->type == 'student') {
            $sql = 'select password from students where email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            $inregistrare = $rez->fetch_assoc();
            return $inregistrare['password'];
        } else if ($this->type == 'teacher') {
            $sql = 'select password from teachers where email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            $inregistrare = $rez->fetch_assoc();
            return $inregistrare['password'];
        }
    }

    public function generateTokenID()
    {
        require '../dbh.inc.php';
        if ($this->type == 'student') {
            $sql = 'select email from students where email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            $inregistrare = $rez->fetch_assoc();
            $token = md5(uniqid($inregistrare['email'], true));
            $date = date('Y-m-d H:i:s');
            $sql = 'UPDATE students SET SessionID="' . $token . '", updated_At="' . $date . '" WHERE email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rezultat->close();
            return $token;
        } else if ($this->type == 'teacher') {
            $sql = 'select email from teachers where email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            $inregistrare = $rez->fetch_assoc();
            $token = md5(uniqid($inregistrare['email'], true));
            $date = date('Y-m-d H:i:s');
            $sql = 'UPDATE teachers SET SessionID="' . $token . '", updated_At="' . $date . '" WHERE email = ?';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('s', $this->mail);
            $rezultat->execute();
            $rezultat->close();
            return $token;
        }
    }
}
