<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Register page</title>
    <link rel="stylesheet" type="text/css" href="../../css/login&register/register.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>

    <div class="wrapper">
        <div class="login-box">
            <div class="sign-in">Register </div>

            <form class="formstyle" action="../controllers/register.php" method="POST">
                <div class="textboxNumePrenume">
                    <div class="textbox">
                        <i class="fas fa-user"></i>
                        <input type="text" name="last_name" placeholder="Last Name"  required="required">
                    </div>
                    <div class="textbox">
                        <i class="fas fa-user"></i>
                        <input type="text" name="first_name" placeholder="First Name"  required="required">
                    </div>
                </div>

                <div class="textbox">
                    <i class="fas fa-birthday-cake"></i>
                    <input name="date" type="date"  required="required">
                </div>

                <div class="textbox">
                    <i class="fas fa-envelope"></i>
                    <input type="text" name="mail" placeholder="Email address"  required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Invalid Email Format">
                </div>


                <div class="textbox">
                    <i class="fas fa-key"></i>
                    <input type="password" name="password" placeholder="Password"  required="required">
                </div>

                <div class="textbox">
                    <i class="fas fa-key"></i>
                    <input type="password" name="cpassword" placeholder="Confirm password"  required="required">
                </div>
                <div class="textbox">
                    <i class="fas fa-user"></i>
                    <input type="text" name="type" placeholder="Unique code"  required="required">
                </div>


                <input type="submit" class="href-button" value="Sign up" name="signup">
            </form>
            <hr>
            <?php
            if (isset($samePasswords) && $samePasswords == "false" && isset($existentEmail) && $existentEmail == "true") {
                echo ' <div class="trouble">Passwords are not the same and this email already exists!</div>';
                echo '<hr>';
            } else
            if (isset($samePasswords) && $samePasswords == "false") {
                echo ' <div class="trouble">Passwords are not the same!</div>';
                echo '<hr>';
            } else
            if (isset($existentEmail) && $existentEmail == "true") {
                echo ' <div class="trouble">This email already exists!</div>';
                echo '<hr>';
            } else
            if (isset($register) && $register == "succes") {
                echo ' <div class="trouble">Your account was successfully created!</div>';
                echo ' <a href="../views/login.php" class="trouble" >Go to login!</a>';
                echo '<hr>';
            }
            ?>
        </div>
    </div>

</body>

</html>