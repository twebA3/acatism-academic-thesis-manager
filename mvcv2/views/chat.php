<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Student - Panel</title>
    <link rel="stylesheet" href="../../css/chat-style.css">
    <link rel="stylesheet" href="../../css/dashboard-style.css">
    <link rel="stylesheet" href="../../css/student/dashboard.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body class="Site">
    <?php
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    if ($_SESSION['type'] === "student")
        include_once('header.php');
    else
        include_once('header-teacher.php');
    ?>

    <main class="Site-content">


        <article class="conversatie">

            <div id="continut">

            </div>

            <div class="input-box">
                <input type="text" class="input-text" style='width:70%; height:50%;' />
                <button id="submit-button">Send</button>
            </div>

        </article>


        <aside class="lista-conversatii">
            <?php
            session_start();
            $urlConv = 'http://localhost/acatism-academic-thesis-manager/mvcv2/controllers/get-conversations.php';
            $urlConv = $urlConv . '?email=' . $_SESSION['mail'] . '&type=' . $_SESSION['type'];


            $c = curl_init();

            // stabilim URL-ul serviciului Web invocat
            curl_setopt($c, CURLOPT_URL, $urlConv);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

            $lista = json_decode(curl_exec($c));

            foreach ($lista as $cheie => $valoare) {
                echo '<button class="profesor" id="conv-' . $valoare . '">'  . $cheie . '</button>';
            }

            curl_close($c);
            ?>
        </aside>

    </main>

    <footer>Copyright &#169; 2019</footer>
    <script>
        var butonApasat;
        var headerTag = document.getElementsByTagName('header');
        var h1Tag = headerTag[0].getElementsByTagName('h1');
        var userType = h1Tag[0].innerText.split(' - ')[1].toLowerCase();


        const trimtieMesaj = ev => {
            var input = ev.target.previousSibling;
            while (input.nodeName == "#text") input = input.previousSibling;
            let msg = `{ "email": "${butonApasat.innerText}", "message": "${input.value}" }`;
            console.log(msg);

            let request = new Request('../controllers/send-message.php', {
                method: 'POST',
                body: JSON.stringify(msg), // convertim datele JSON in sir de caractere
                headers: {} // n-avem campuri-antet
            });
            fetch(request) // promitem sa executam codul daca e succes...
                .then(response => {
                    console.log(response);
                    // verificam daca am primit date JSON de la server 
                    var contentType = response.headers.get('Content-Type');
                    console.log(contentType);
                    if (contentType && contentType.includes('application/json')) {
                        return response.json();
                    };
                    throw new TypeError('Datele primite nu-s JSON :(');
                })
                // procesam efectiv datele
                .then(json => {
                    // cream un nod text care indica tasta apasata
                    // raportam datele primite si la consola browser-ului
                    console.log(json);
                })
                .catch(error => {
                    console.log(error);
                });

            var nouText = document.createElement("div");
            
            //equire('dateformat');
            var now = new Date();
            var luna = (now.getMonth()+1) < 10 ? ('0' + (now.getMonth()+1)) : (now.getMonth()+1); 
            var ziua = now.getDate() < 10 ? ('0' + now.getDate()) : now.getDate(); 
            var ora = now.getHours() < 10 ? ('0' + now.getHours()) : now.getHours(); 
            var minute = now.getMinutes() < 10 ? ('0' + now.getMinutes()) : now.getMinutes();
            var secunde = now.getSeconds() < 10 ? ('0' + now.getSeconds()) : now.getSeconds();

            var date = now.getFullYear() + '-' + luna  + '-' + ziua + ' ' + ora + ':' + minute + ':' + secunde;
            nouText.innerText = "me" + " [" + date + "] : " + input.value;
            nouText.className = "mesaj";
            document.getElementById("continut").appendChild(nouText);
        }

        document.getElementById("submit-button").addEventListener('click', trimtieMesaj);

        const trateazaEv = ev => {
            butonApasat = ev.target;
            var elemMesaje = document.getElementsByClassName("mesaj");
            while (elemMesaje.length > 0) elemMesaje[0].remove();
            var id = ev.target.id.split('-')[1];
            let request = new Request('../controllers/get-messages.php?id_conv=' + id, {
                method: 'GET',
                headers: {} // n-avem campuri-antet
            });
            fetch(request) // promitem sa executam codul daca e succes...
                .then(response => {
                    // verificam daca am primit date JSON de la server 
                    var contentType = response.headers.get('Content-Type');
                    if (contentType && contentType.includes('application/json')) {
                        return response.json();
                    };
                    throw new TypeError('Datele primite nu-s JSON :(');
                })
                // procesam efectiv datele
                .then(json => {
                    // cream un nod text care indica tasta apasata
                    var continut = document.getElementById("continut");
                    for (var i = 0; i < json.length; i++) {
                        var nouText = document.createElement("div");
                        if (json[i][1].localeCompare(userType) == 0)
                            nouText.innerText = "me" + " [" + json[i][2] + "] : " + json[i][0];
                        else
                            nouText.innerText = butonApasat.innerText + " [" + json[i][2] + "] : " + json[i][0];
                        nouText.className = "mesaj";
                        continut.appendChild(nouText);
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        }

        var buttons = document.getElementsByClassName("profesor");

        for (var i = 0; i < buttons.length; i++) {
            var id = buttons[i].id.split('-')[1];
            console.log(id);
            buttons[i].addEventListener('click', trateazaEv);

        }

        function openPage(pageName, elmnt, color) {
            // Hide all elements with class="tabcontent" by default */
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Remove the background color of all tablinks/buttons
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].style.backgroundColor = "";
            }

            // Show the specific tab content
            document.getElementById(pageName).style.display = "block";

            // Add the specific color to the button used to open the tab content
            elmnt.style.backgroundColor = color;
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();


        function showNotification() {
            console.log('salut');
            document.getElementById("myDropdown").classList.toggle("show");
        }


        window.onclick = function(event) {
            if (!event.target.matches('.dropbtn')) {
                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
        /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
        function showMobileNav() {

            var x = document.getElementById("top-nav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
</body>

</html>