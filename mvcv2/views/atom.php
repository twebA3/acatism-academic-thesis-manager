<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Alerts about your project</title>
    <link href="http://acatism.org/"/>
    <updated><?=$alerts[0]['updated_at']?></updated>
    <author>
        <name><?=$teacherName?></name>
    </author>
    <id><?=$_GET['id']?></id>
<?php
foreach($alerts as $alert)
 { 
    echo "\t<entry>\n";
    echo "\t\t<title>".$alert['message']."</title>\n";
    echo "\t\t<id>".$alert['id']."</id>\n";
    echo "\t\t<updated>".$alert['updated_at']."</updated>\n";
    echo "\t</entry>\n";
 }
?>
</feed>