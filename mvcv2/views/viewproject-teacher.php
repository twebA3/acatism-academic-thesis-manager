<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Teacher - Panel</title>
  <link rel="stylesheet" href="../../css/dashboard-style.css">
  <link rel="stylesheet" href="../../css/student/profile.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    .btn {
      background-color: #11998e;
      float: right;
      border: none;
      color: black;
      padding: 12px 16px;
      font-size: 16px;
      cursor: pointer;
    }

    .btn:hover {
      background-color: yellowgreen;
    }

    .btn1 {
      background-color: white;
      float: right;
      border: none;
      color: black;
      padding: 12px 16px;
      font-size: 16px;
      cursor: pointer;
    }

    .btn1:hover {
      background-color: #11998e;
    }

    .addbutton {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }

    .btn2 {
      background-color: #11998e;

      border: none;
      color: black;
      padding: 12px 16px;
      font-size: 16px;
      cursor: pointer;
    }

    .btn2:hover {
      background-color: white;
    }

    .btn1:disabled {
      background-color: #11998e;
    }

    .form-row>label {
      padding: .5em 1em .5em 0;
      flex: 1;
    }

    .form-row>input,
    .form-row>.custom-input {
      flex: 2;
    }

    .form-row>input,
    .form-row>textarea {
      padding: .5em;
    }
    .btnadd {
      display: flex;
      justify-content: center;
    }



    @media screen and (min-width: 1200px) {

      .form-row>input,
      .form-row>.custom-input {
        flex: 5;
      }
    }
  </style>
</head>

<body class="Site">
  <header>

    <a href=""> <img src="../../images/app-logo.png" alt="logo" />
      <h1> AcaTisM - Teacher
      </h1>
    </a>
    <nav class="menu-items">

      <a href="edit-profile-student.php"><span>Edit Profile</span><i class="fas fa-user"></i></a>
      <a href="destroy-session.php"><span>Sign out</span><i class="fas fa-sign-out-alt"></i></a>
    </nav>
  </header>
  <nav class="topnav" id="top-nav">
    <a href="createproject-teacher.php">Create project</a>
    <a href="../controllers/viewallprojects-teacher.php">All projects</a>
    <a href="../controllers/viewprojects-teacher.php">My projects</a>
    <a href="#" class="active">Project Details</a>
    <a href="../controllers/chat.php">Chat</a>
    <a class="icon" onclick="showMobileNav()">
      <i class="fa fa-bars"></i>
    </a>
  </nav>

  <main class="Site-content">
  <h2 style="text-align: center;">Project details</h2>
    <?php echo $projects; ?>

    <h2 style="text-align: center;">Add Tasks</h2>
    <div class="project" style="background-color: #11998e;">
      <form class="addbutton" action="../controllers/viewproject-teacher.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <li class="form-row">
          <label for="bio">Description</label>
          <textarea class="custom-input" id="bio" name="description" required="required"></textarea>
        </li>
        <li class="form-row">
          <label for="bio">Deadline</label>
          <input name="deadline" type="date" required="required">
        </li>
        <li class="form-row">
          <label for="interests">Github</label>
          <select name="github" style="width: 82.5%;"  required="required">
            <option value="1">I want to use github</option>
            <option value="0">I don't want to use github</option>
          </select>
        </li>
      
          <div class="btnadd">
            <button name="addtask" class="btn2"><i class="fa fa-plus"></i> Add Task</button>
          </div>
        
      </form>
    </div>
    <h2 style="text-align: center;">Tasks</h2>
    <?php echo $tasks; ?>

  </main>
  <footer>Copyright &#169; 2019</footer>
  <script>
    function openPage(pageName, elmnt, color) {
      // Hide all elements with class="tabcontent" by default */
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }

      // Remove the background color of all tablinks/buttons
      tablinks = document.getElementsByClassName("tablink");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
      }

      // Show the specific tab content
      document.getElementById(pageName).style.display = "block";

      // Add the specific color to the button used to open the tab content
      elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


    function showNotification() {
      document.getElementById("myDropdown").classList.toggle("show");
    }


    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
    function showMobileNav() {

      var x = document.getElementById("top-nav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }
  </script>
</body>

</html>