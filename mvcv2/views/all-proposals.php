<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Student - Panel</title>
    <link rel="stylesheet" href="../../css/dashboard-style.css">
  <link rel="stylesheet" href="../../css/student/dashboard.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body class="Site">
    <?php
    include_once('header.php');
    ?>

    <main class="Site-content">

        <article>
            <header>
                <h2>All proposals</h2>
                <p>Below you have a list of all the projects proposed by the teachers that are still looking for students</p>
            </header>
            Filter by interest: 
            <form action="all-proposals.php" method="get">
   interest: <input type="text" name="theme"><br>
  <input type="submit" value="Submit">
</form> 

            <table>
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Project Description</th>
                        <th>Teacher</th>
                        <th>Type</th>
                        <th>Interest</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(sizeof($projects)==0)
                {
                    echo 'No projects found based on your filters';
                }
                foreach($projects as $project)
                {
                    echo "<tr>";
                    echo "<td>".$project['name']."</td>";
                    echo "<td>".$project['description']."</td>";
                    echo "<td>".$project['teacher_name']."</td>";
                    echo "<td>".$project['type']."</td>";
                    echo "<td>".$project['theme']."</td>";
                    echo "<td><button class=\"button\">Send message</button></td>";
                    echo "</tr>";
                }

                ?>
                </tbody>
            </table>
        </article>


    </main>
    <footer>Copyright &#169; 2019</footer>
    <script>
        const mesajDummy = ev => {
      var parinte = ev.target.parentNode;
      var nodMail = parinte.previousSibling.previousSibling;
      let msg = `{ "email": "${nodMail.innerText}", "message": "Buna ziua! As dori sa colaborez cu dumneavoastra!" }`;
      console.log(msg);

      let request = new Request('../controllers/send-message.php', {
        method: 'POST',
        body: JSON.stringify(msg), // convertim datele JSON in sir de caractere
        headers: {} // n-avem campuri-antet
      });
      fetch(request) // promitem sa executam codul daca e succes...
        .then (response => {
          console.log(response);
        // verificam daca am primit date JSON de la server 
        var contentType = response.headers.get('Content-Type');
        console.log(contentType);
          if (contentType && contentType.includes('application/json')) {
            return response.json();
          };
          throw new TypeError ('Datele primite nu-s JSON :(');
        })
      // procesam efectiv datele
        .then (json => {
          // cream un nod text care indica tasta apasata
          // raportam datele primite si la consola browser-ului
          console.log(json);
        })
        .catch(error => {
          console.log(error);
        });
    }

    var butoane = document.getElementsByClassName("button");
    for(var i = 0; i < butoane.length; i++) {
      butoane[i].addEventListener('click', mesajDummy);
    }
        function openPage(pageName, elmnt, color) {
            // Hide all elements with class="tabcontent" by default */
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Remove the background color of all tablinks/buttons
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].style.backgroundColor = "";
            }

            // Show the specific tab content
            document.getElementById(pageName).style.display = "block";

            // Add the specific color to the button used to open the tab content
            elmnt.style.backgroundColor = color;
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();


        function showNotification() {
            document.getElementById("myDropdown").classList.toggle("show");
        }


        window.onclick = function (event) {
            if (!event.target.matches('.dropbtn')) {
                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
        /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
        function showMobileNav() {

            var x = document.getElementById("top-nav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        } 
    </script>
</body>

</html>