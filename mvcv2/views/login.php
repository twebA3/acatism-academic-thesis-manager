<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Login page</title>
    <link rel="stylesheet" type="text/css" href="../../css/login&register/login.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>

    <div class="wrapper">
        <div class="login-box">
            <div class="sign-in">Login </div>
            <form class="formstyle" action="../controllers/login.php" method="POST">


                <div class="textbox">
                    <i class="fas fa-envelope"></i>
                    <input type="text" name="mail" placeholder="Email address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Invalid Email Format">
                </div>

                <div class="textbox">
                    <i class="fas fa-key"></i>
                    <input type="password" name="pass" placeholder="Password" pattern=".{3,}" title="Six or more characters">
                </div>
                <div class="selecttext">
                    <select name="type">
                        <option value="student">Student</option>
                        <option value="teacher">Teacher</option>
                    </select>
                </div>
                <hr>
                <input type="submit" class="href-button" value="Sign in" name="signin">
            </form>
            <div class="textbox">
                <a href="register.php" class="trouble">Do not you have an account?</a>

                <!-- <div class="modal" id="newpass">
                    <div class="modal-container">
                        <div class="textbox">
                            <i class="fas fa-envelope"></i>
                            <input type="text" placeholder="Email address">
                        </div>
                        <a href="#modal-close">Close</a>
                    </div>
                </div> -->
            </div>
            <hr>
          <!-- <a href="register.php" class="href-button">Create Account</a> -->
          
            <?php
          
                    if(isset($status) && $status == "failed")
                    echo ' <div class="trouble">Type, email or password is wrong! </div>';
                    echo '<hr>';
            ?>
        </div>
    </div>

</body>

</html>