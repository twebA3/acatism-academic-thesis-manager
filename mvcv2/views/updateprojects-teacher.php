<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Teacher - Panel</title>
  <link rel="stylesheet" href="../../css/dashboard-style.css">
  <link rel="stylesheet" href="../../css/student/profile.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    .btn {
      background-color: DodgerBlue;
      float: right;
      border: none;
      color: white;
      padding: 12px 16px;
      font-size: 16px;
      cursor: pointer;
    }

    .btn:hover {
      background-color: RoyalBlue;
    }
  </style>
</head>

<body class="Site">
  <header>

    <a href="logo"> <img src="../../images/app-logo.png" alt="logo" />
      <h1> AcaTisM - Teacher
      </h1>
    </a>
    <nav class="menu-items">

     
      <a href="edit-profile-students.php"><span>Edit Profile</span><i class="fas fa-user"></i></a>
      <a href="destroy-session.php"><span>Sign out</span><i class="fas fa-sign-out-alt"></i></a>
    </nav>
  </header>
  <nav class="topnav" id="top-nav">
    <a href="createproject-teacher.php">Create project</a>
    <a href="../controllers/viewallprojects-teacher.php">All projects</a>
    <a href="../controllers/viewprojects-teacher.php">My projects</a>
    <a href="../controllers/updateprojects-teacher.php" class="active">Edit project</a>
    <a href="../controllers/chat.php">Chat</a>
    <a class="icon" onclick="showMobileNav()">
      <i class="fa fa-bars"></i>
    </a>
  </nav>

  <main class="Site-content">
    <form action="../controllers/updateprojects-teacher.php?id=<?php echo $_GET['id']; ?>" , method="POST">
      <ul class="form-wrapper">
        <li class="form-row">
          <label for="name">Email Student</label>
          <input type="text" id="id_student" name="id_student" required="required">
        </li>
        <li class="form-row">
          <label for="name" >Name</label>
          <input type="text" id="name" name="name" required="required">
        </li>
        <li class="form-row">
          <label for="bio">Description</label>
          <textarea class="custom-input" id="bio" name="description" required="required"></textarea>
        </li>
        <li class="form-row">
          <label for="interests">Type</label>
          <select name="type" style="width: 82.5%;" required="required">
            <option value="licenta">Licenta</option>
            <option value="master">Master</option>
          </select>

        </li>
        <li class="form-row">
          <label for="interests">Theme</label>
          <select name="theme" style="width: 82.5%;" required="required">
            <option value="Tehnologii web">Tehnologii web</option>
            <option value="Procesare imagini">Procesare imagini</option>
            <option value="Inteligenta artificiala">Inteligenta artificiala</option>
            <option value="Algoritmi genetici">Algoritmi genetici</option>
          </select>

        </li>
        <li class="form-row">
          <label for="old_password">Year</label>
          <input type="text" id="old_password" name="year" pattern="[0-9]{4}" title="Invalid year" required="required">
        </li>
        <li class="form-row">
          <button type="submit" class="button" name="submit">Submit</button>
        </li>
      </ul>
    </form>
    <?php if (isset($email_error) && $email_error == "true")
            echo "<h3>Student email is wrong!</h3>";
      ?>
  </main>
  <footer>Copyright &#169; 2019</footer>
  <script>
    function openPage(pageName, elmnt, color) {
      // Hide all elements with class="tabcontent" by default */
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }

      // Remove the background color of all tablinks/buttons
      tablinks = document.getElementsByClassName("tablink");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
      }

      // Show the specific tab content
      document.getElementById(pageName).style.display = "block";

      // Add the specific color to the button used to open the tab content
      elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


    function showNotification() {
      document.getElementById("myDropdown").classList.toggle("show");
    }


    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
    function showMobileNav() {

      var x = document.getElementById("top-nav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }
  </script>
</body>

</html>