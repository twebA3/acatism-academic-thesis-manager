<header>

    <a href=""> <img src="../../images/app-logo.png" alt="logo" />
      <h1> AcaTisM - Teacher
      </h1>
    </a>
    <nav class="menu-items">

     
      <a href="../controllers/edit-profile-student.php"><span>Edit Profile</span><i class="fas fa-user"></i></a>
      <a href="../controllers/destroy-session.php"><span>Sign out</span><i class="fas fa-sign-out-alt"></i></a>
    </nav>
  </header>
  <nav class="topnav" id="top-nav">
    <a href="../controllers/createproject-teacher.php">Create project</a>
    <a href="../controllers/viewallprojects-teacher.php">All projects</a>
    <a href="../controllers/viewprojects-teacher.php">My projects</a>
    <a href="../controllers/updateprojects-teacher.php">Edit project</a>
    <a href="../controllers/chat.php">Chat</a>

    <a class="icon" onclick="showMobileNav()">
      <i class="fa fa-bars"></i>
    </a>
  </nav>