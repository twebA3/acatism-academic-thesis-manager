<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="UTF-8">
  <title>Student - Panel</title>
  <link rel="stylesheet" href="../../css/dashboard-style.css">
  <link rel="stylesheet" href="../../css/student/profile.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body class="Site">

    <?php
    if($_SESSION['type'] == "student")
        include_once('header.php');
    else
      include_once('header-teacher.php');
  ?>
  <main class="Site-content">

    <section>
      <h2>Edit profile</h2>

      <form action="../controllers/edit-profile-student.php" method="POST">
        <ul class="form-wrapper">
          <li class="form-row">
            <label for="name">First Name</label>
            <input type="text" name="name">
          </li>
          <li class="form-row">
            <label for="townborn">Last Name</label>
            <input type="text" name="townborn">
          </li>
          <li class="form-row">
            <label for="birthday">Birthday</label>
            <input type="date" name="birthday">
          </li>
          <li class="form-row">
            <label for="bio">Biography</label>
            <textarea class="custom-input" name="bio"></textarea>
          </li>
          <li class="form-row">
            <label for="old_password">Old password</label>
            <input type="password" name="old_password">
          </li>
          <li class="form-row">
            <label for="new_password1">New password</label>
            <input type="password" name="new_password1">
          </li>
          <li class="form-row">
            <label for="new_password2">New password again</label>
            <input type="password" name="new_password2">
          </li>
          <li class="form-row">
            <button type="submit" class="button">Submit</button>
          </li>
        </ul>
      </form>
      <?php
        if(isset($message))
        echo ' <p>' . $message .'</p>';
            ?>
    </section>
  </main>

  <footer>Copyright &#169; 2019</footer>
  <script>



    function openPage(pageName, elmnt, color) {
      // Hide all elements with class="tabcontent" by default */
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }

      // Remove the background color of all tablinks/buttons
      tablinks = document.getElementsByClassName("tablink");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
      }

      // Show the specific tab content
      document.getElementById(pageName).style.display = "block";

      // Add the specific color to the button used to open the tab content
      elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


    function showNotification() {
      console.log('salut');
      document.getElementById("myDropdown").classList.toggle("show");
    }


    window.onclick = function (event) {
      if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
    function showMobileNav() {

      var x = document.getElementById("top-nav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }


    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
  </script>
</body>

</html>