<header>
    <a href=""> <img src="../../images/app-logo.png" alt="logo" />
        <h1> AcaTisM - Student
        </h1>
    </a>
    <nav class="menu-items">
        <a href="edit-profile-student.php"><span>Edit Profile</span><i class="fas fa-user"></i></a>
        <a href="destroy-session.php"><span>Sign out</span><i class="fas fa-sign-out-alt"></i></a>
    </nav>
</header>
<nav class="topnav" id="top-nav">
    <a href="dashboard-student.php">My project</a>
    <a href="all-teachers.php">Teachers List</a>
    <a href="all-proposals.php">Projects Proposal List</a>
    <a href="chat.php">Chat</a>
    <a class="icon" onclick="showMobileNav()">
        <i class="fa fa-bars"></i>
    </a>
</nav>