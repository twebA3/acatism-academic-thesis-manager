<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="UTF-8">
  <title>Student - Panel</title>
  <link rel="stylesheet" href="../../css/dashboard-style.css">
  <link rel="stylesheet" href="../../css/student/dashboard.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body class="Site">

  <?php
  include_once('header.php');
  ?>
  <main class="Site-content">
    <article>
      <header>
        <h2>My project: <?=$project['name']?></h2>
        <p><?=$project['description']?></p>
      </header>
      <div class="project-data">

        <button class="tablink" onclick="openPage('Timeline', this, 'white')" id="defaultOpen">Timeline</button>
        <button class="tablink" onclick="openPage('Update', this, 'white')">Update</button>
        <button class="tablink" onclick="openPage('Changes', this, 'white')">Changes</button>

        <section id="Timeline" class="tabcontent">
          <header>
            <h3>Timeline</h3>
            <p>The most important tasks you must do in order to finish the project</p>
          </header>
          <article>
            <?php
            if(sizeof($tasks)==0)
              echo 'There are no tasks yet';
            foreach($tasks as $task)
            {
                $disabled = $task['github'] === 1 ? 'disabled' : '';
                $done = $task['done'] === 1 ? ' checked' : '';
                echo 'Task: '.$task['description'];
                echo "<br>Deadline: ".$task['deadline'];
                if($task['github']===1)
                {
                  if($task['done'] === 0)
                    {
                      echo "<br>Status: Pending";
                      echo '<br>To resolve this task you have to make a commit on your repository and in the message include this string: "['.$task['id'].']"';}
                  else
                    echo "<br>Status: Done using github";
                }
                else if($task['done'] === 0)
                {
                  echo "<br>Status: Pending";
                       // echo "<input type='checkbox' id='myCheck' disabled".$done.">".$task['description']."<br>";
                  echo '<form action="../controllers/dashboard-student.php?taskid='.$task['id'].'" method="POST" class="task">';
                  echo '<button  name="submit" value="task" type="submit" style="color:#333652">Mark as done</button>';
                  echo '</form>';
                }
                else
                {
                  echo "<br>Status: Done";
                }
                echo "<br><br>";
            }
            ?>
            
          </article>
        </section>

        <section id="Update" class="tabcontent">
          <header>
            <h3>Update</h3>
            <p>Here you can set your github repo and upload files manually in order to update the progress of your
              project</p>
          </header>
          <article>
            <h4>Source control - automatically updates</h4>
            <form action="../controllers/dashboard-student.php" method="POST" class="github">
                <div>
                    <label for="github">Github repo link* </label>
                    <input id="github" type="text" name="github" value="<?=$project['github']?>" required>
                </div>
                
                <button  name="submit" value="github" type="submit" class="button">  Submit </button>
            </form>
            <h4>Manual updates</h4>
            <div id="form">
              <label for="fileupload"> Select a file to upload</label>
              <input type="file" name="fileupload" id="fileupload">
              <div>
                <!--<button class="button">Check if raport is valid</button>-->
                <button type="submit" class="button" id="submit-button">Submit files</button></div>

            </div>
            <h4>Checked files</h4>
            <ul id="up-files">
            </ul>
          </article>
        </section>

        <section id="Changes" class="tabcontent">
          <header>
            <h3>Changes</h3>
            <p>You will recieve the changes on your email and on the following atom feed: 
            <a href="http://localhost/acatism-academic-thesis-manager/mvcv2/controllers/atom.php?id=<?=$project['id']?>">Atom feed</a></p>
          </header>
        </section>
      </div>
    </article>
  </main>
  <footer>Copyright &#169; 2019</footer>
  <script type="application/javascript">
    function openPage(pageName, elmnt, color) {
      // Hide all elements with class="tabcontent" by default */
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }

      // Remove the background color of all tablinks/buttons
      tablinks = document.getElementsByClassName("tablink");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
      }

      // Show the specific tab content
      document.getElementById(pageName).style.display = "block";

      // Add the specific color to the button used to open the tab content
      elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


    function showNotification() {
      console.log('salut');
      document.getElementById("myDropdown").classList.toggle("show");
    }


    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
    function showMobileNav() {

      var x = document.getElementById("top-nav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }


    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }

    // Tratam evenimentul de apasare a unei taste	
    /*const trateazaEveniment = ev => {
        
        // mesajul propriu-zis trimis serverului prin POST 
        // atunci cand survine evenimentul
        let msg = `{ "tasta": "${String.fromCharCode (ev.charCode)}", "data": "${Date.now()}" }`; 
        // incapsulam o cerere POST
        let request = new Request('post.php', {
              method: 'POST',
              body: JSON.stringify (msg), // convertim datele JSON in sir de caractere
              headers: {}					// n-avem campuri-antet
        });
        fetch(request) // promitem sa executam codul daca e succes...
            .then (response => {
                // verificam daca am primit date JSON de la server 
                var contentType = response.headers.get('Content-Type');
                if (contentType && contentType.includes('application/json')) {
                    return response.json();
                };
                throw new TypeError ('Datele primite nu-s JSON :(');
              })
            // procesam efectiv datele
              .then (json => {
                  // cream un nod text care indica tasta apasata
                  let elem = document.createTextNode (json.tasta);
                  document.getElementById ('tasteApasate').appendChild (elem);
                  // raportam datele primite si la consola browser-ului
                  console.log (`Date JSON primite: tasta=${json.tasta}, data=${Date(json.data)}`); 
              })
            .catch (error => { console.log (error); }); // a survenit o eroare
    };
    /*const trateazaEveniment = ev => {
                
                // mesajul propriu-zis trimis serverului prin POST 
                // atunci cand survine evenimentul
                let msg = `{ "tasta": "${String.fromCharCode (ev.charCode)}", "data": "${Date.now()}" }`; 
                // incapsulam o cerere POST
                let request = new Request('post.php', {
                      method: 'POST',
                      body: JSON.stringify (msg), // convertim datele JSON in sir de caractere
                      headers: {}					// n-avem campuri-antet
                });
                fetch(request) // promitem sa executam codul daca e succes...
                    .then (response => {
                        // verificam daca am primit date JSON de la server 
                        var contentType = response.headers.get('Content-Type');
                        if (contentType && contentType.includes('application/json')) {
                            return response.json();
                        };
                        throw new TypeError ('Datele primite nu-s JSON :(');
                      })
                    // procesam efectiv datele
                      .then (json => {
                          // cream un nod text care indica tasta apasata
                          let elem = document.createTextNode (json.tasta);
                          document.getElementById ('tasteApasate').appendChild (elem);
                          // raportam datele primite si la consola browser-ului
                          console.log (`Date JSON primite: tasta=${json.tasta}, data=${Date(json.data)}`); 
                      })
                    .catch (error => { console.log (error); }); // a survenit o eroare
            };
            
            document.addEventListener ('keypress', trateazaEveniment);*/

    document.getElementById("submit-button").addEventListener('click', printDoc);

    function printDoc() {
      console.log("sunt aci");
      var file = document.getElementById("fileupload");
      var lista = document.getElementById("up-files");

      var nouText = document.createElement("li");

      var dt = new Date();
      var utcDate = dt.toUTCString();
      nouText.innerText = file.files[0].name + "  " + utcDate;
      lista.appendChild(nouText);
    }

    document.getElementById("fileupload").addEventListener('change', validateDoc);

    /* function */
    function validateDoc() {
      var succes = document.getElementsByClassName("verde");
      while (succes.length > 0) succes[0].remove();

      var erori = document.getElementsByClassName("eroare");
      while (erori.length > 0) erori[0].remove();

      var warning = document.getElementsByClassName("warning");
      while (warning.length > 0) warning[0].remove();

      console.log('Horray! Someone changed file');
      var reader = new FileReader();
      var inp = document.getElementById("fileupload");
      console.log(inp.files[0].type);
      if (inp.files[0].name.split('.').pop() != "html") {
        var nouText = document.createElement("p");
        nouText.innerText = "Fisierul nu are formatul html, deci nu va fi verificat.";
        nouText.className = "verde";
        inp.parentElement.appendChild(nouText);
      } else {
        reader.readAsText(inp.files[0], "UTF-8");
        reader.onload = function(evt) {
          let request = new Request('../controllers/validator.php', {
            method: 'POST',
            body: evt.target.result, // convertim datele JSON in sir de caractere
            headers: {} // n-avem campuri-antet
          });
          fetch(request) // promitem sa executam codul daca e succes...
            .then(response => {
              // verificam daca am primit date JSON de la server 
              var contentType = response.headers.get('Content-Type');
              if (contentType && contentType.includes('application/json')) {
                return response.json();
              };
              throw new TypeError('Datele primite nu-s JSON :(');
            })
            // procesam efectiv datele
            .then(json => {
              // cream un nod text care indica tasta apasata
              if (json.Errors.length === 0 && json.Warnings.length === 0) {
                var nouText = document.createElement("p");
                nouText.innerText = "Fisierul este corect";
                nouText.className = "verde";
                inp.parentElement.appendChild(nouText);
              } else {
                for (var i = 0; i < json.Errors.length; i++) {
                  var nouText = document.createElement("p");
                  nouText.innerText = json.Errors[i];
                  nouText.className = "eroare";
                  inp.parentElement.appendChild(nouText);
                }
                for (var i = 0; i < json.Warnings.length; i++) {
                  var nouText = document.createElement("p");
                  nouText.innerText = json.Warnings[i];
                  nouText.className = "warning";
                  inp.parentElement.appendChild(nouText);
                }
              }
              let elem = document.createTextNode(json.Errors[0]);
              console.log(json.Errors);
              // raportam datele primite si la consola browser-ului
              //console.log (`Date JSON primite: tasta=${json.tasta}, data=${Date(json.data)}`); 
            })
            .catch(error => {
              console.log(error);
            }); // a survenit o eroare
          //document.getElementById("fileContents").innerHTML = evt.target.result;
        }
        reader.onerror = function(evt) {
          console.log('teapa');
        }

        //doc.parentElement.appendChild(nouText);
      }
    }
  </script>
</body>

</html>