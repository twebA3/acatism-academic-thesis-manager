<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Main Page</title>
  <link rel="stylesheet" type="text/css" href="../../css/main-page/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>
  <div class="wrapper">
    <nav class="navbar">
      <img class="logo" src="../../images/acatism.png" alt="Logo" height="50">
      <div class="navbar-nav">
        <form class="formstyle" action="../controllers/index.php" method="POST">
          <div class="search-box">
            <input class="search-text" type="text" name="search" placeholder="Type to search">
            <a class="search-button" href="">
              <i class="fas fa-search"></i>
            </a>
          </div>
        </form>
        <a href="#">Home</a>
        <a href="mailto:someone@yoursite.com">Contact</a>
        <a href="../views/index.about.html">About</a>
        <a href="../views/login.php">Login</a>
        <a href="../views/register.php">Register</a>

        <span class="open-slide">
          <a href="#" onclick="openSlideMenu()">
            <i class="fas fa-bars"></i>
          </a>
        </span>
      </div>
    </nav>


    <div id="main-content">
      <div id="side-menu" class="side-nav">

        <a href="#" class="button-close" onclick="closeSlideMenu()">&times;</a>

        <div class="side-nav-content">

          <div class="mobile-elements">
            <form class="formstyle" action="../controllers/index.php" method="POST">
              <div class="side-search-box">
                <a class="side-search-button" href="#">
                  <i class="fas fa-search"></i>
                </a>
                <input class="side-search-text" type="text" name="search" placeholder="Type to search">

              </div>
            </form>
            <a href="#">Home</a>
            <a href="mailto:someone@yoursite.com">Contact</a>
            <a href="../views/index.about.html">About</a>
            <a href="../views/login.php">Login</a>
            <a href="../views/register.php">Register</a>
          </div>



        </div>
      </div>

      <div class="project-content">
        <?php
        if (isset($search))
          echo $search;
        else
          echo $projects; ?>
      </div>
    </div>

    <footer>
      <h2>Copyright &copy; 2019 Academic Thesis Manager</h2>
    </footer>

    <script>
      function openSlideMenu() {
        document.getElementById('side-menu').style.width = '275px';

      }

      function closeSlideMenu() {
        document.getElementById('side-menu').style.width = '0';

      }
    </script>
  </div>
</body>

</html>