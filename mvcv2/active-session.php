<?php

function activeSession($Mail, $TokenID, $Type)
{
        
        require '../dbh.inc.php';
        if ($Type == 'student') {
            $sql = 'select * from students where email = ? and SessionID = ? ';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('ss', $Mail, $TokenID);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            if ($rez->num_rows === 1)

                return "succes";
            else
                header("Location: ../controllers/login.php");
        } else if ($Type == 'teacher') {
            $sql = 'select * from teachers where email = ? and SessionID = ? ';
            $rezultat = $conn->prepare($sql);
            $rezultat->bind_param('ss', $Mail, $TokenID);
            $rezultat->execute();
            $rez = $rezultat->get_result();
            $rezultat->close();
            if ($rez->num_rows === 1)

                return "succes";

            else
                header("Location: ../controllers/login.php");
        }
     
}

function activeSessionforStudent($Type)
{
    if ($Type == 'teacher') 
        header("Location: ../controllers/viewprojects-teacher.php");
}

function activeSessionforTeacher($Type)
{
    if($Type == 'student')
    header("Location: ../controllers/dashboard-student.php");
}

function activeSessionLogin($Mail, $TokenID, $Type)
{
    require '../dbh.inc.php';
    if ($Type == 'student') {
        $sql = 'select * from students where email = ? and SessionID = ? ';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('ss', $Mail, $TokenID);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        if ($rez->num_rows === 1)

            header("Location: dashboard-student.php");
    } else if ($Type == 'teacher') {
        $sql = 'select * from teachers where email = ? and SessionID = ? ';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('ss', $Mail, $TokenID);
        $rezultat->execute();
        $rez = $rezultat->get_result();
        $rezultat->close();
        if ($rez->num_rows === 1)
            header("Location: viewprojects-teacher.php");
    }
}

function destroySession($Mail, $Type)
{
    require '../dbh.inc.php';
    session_destroy();
    if ($Type == 'student') {
        $date = date('Y-m-d H:i:s');
        $sql = 'UPDATE students SET SessionID=0, updated_At="' . $date . '" WHERE email = ?';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $Mail);
        $rezultat->execute();
        $rezultat->close();
        header("Location: ../controllers/login.php");
    } else if ($Type == 'teacher') {
        $date = date('Y-m-d H:i:s');
        $sql = 'UPDATE teachers SET SessionID=0, updated_At="' . $date . '" WHERE email = ?';
        $rezultat = $conn->prepare($sql);
        $rezultat->bind_param('s', $Mail);
        $rezultat->execute();
        $rezultat->close();
        header("Location: ../controllers/login.php");
    }
}
