<?php
include_once '../models/register.php';


session_start();



if (isset($_POST['signup'])) {
    $register = new Register($_POST['last_name'], $_POST['first_name'], $_POST['date'], $_POST['mail'], $_POST['password'], $_POST['cpassword'], $_POST['type']);
    $TypeOfUniqueToken= $register->getTypeOfUniqueToken($register->existentUniqueToken());
    $samePasswords = $register->verifyPasswords();
    $existentEmail = $register->existentEmail($TypeOfUniqueToken);
    $register=$register->register($samePasswords,$existentEmail,$TypeOfUniqueToken);

}

include '../views/register.php';
