<?php

require '../models/conversation.php';

$result = array();
if(isset($_GET['id_conv']))
    $result = getMessages($_GET['id_conv']);

header('Content-type:application/json;charset=utf-8');
echo json_encode($result);
?>