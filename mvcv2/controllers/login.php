<?php
include_once '../models/login.php';
include_once '../active-session.php';

session_start();
if (isset($_SESSION['mail']) && isset($_SESSION['token']) && isset($_SESSION['type']))
activeSessionLogin($_SESSION['mail'],$_SESSION['token'],$_SESSION['type']);




if (isset($_POST['signin'])) {
    $login = new Login($_POST['mail'], $_POST['pass'], $_POST['type']);
    $status = $login->login($login->getHashedPassword());
   
    if ($status == "succes" && $_POST['type']=="student") {
     $_SESSION["token"]=$login->generateTokenID();
     $_SESSION["type"]=$_POST['type'];
     $_SESSION["mail"]=$_POST['mail'];
     header("Location: dashboard-student.php");
    }

    if ($status == "succes" && $_POST['type']=="teacher") {
        $_SESSION["token"]=$login->generateTokenID();
        $_SESSION["type"]=$_POST['type'];
        $_SESSION["mail"]=$_POST['mail'];
        header("Location: viewprojects-teacher.php");
       }
}


include '../views/login.php';
