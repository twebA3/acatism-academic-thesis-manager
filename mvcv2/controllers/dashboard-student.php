<?php
include_once '../active-session.php';
session_start();
activeSessionforStudent($_SESSION['type']);
activeSession($_SESSION['mail'],$_SESSION['token'],$_SESSION['type']);


include_once '../models/project.php';

$project = Project::getProjecByEmail($_SESSION['mail']);
include_once '../models/task.php';
$tasks = Task::getAllByProject($project['id']);

if(isset($_POST['submit'])) {
    switch($_POST['submit']) {
        case 'github':
            Project::updateGithub($project['id'],$_POST['github']);
            break;
        case 'task':
            Task::markAsDone($_GET['taskid'],$project['id']);
            header("Location: dashboard-student.php");
            break;
            
    }
}

include_once '../views/dashboard-student.php';
?>