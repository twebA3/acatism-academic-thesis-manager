<?php

require '../models/conversation.php';

$result = array();
if(isset($_GET['email']) && isset($_GET['type']))
    $result = getConversations(getField('id', 'email', $_GET['email'], $_GET['type'] . 's'), $_GET['type']);

header('Content-type:application/json;charset=utf-8');
echo json_encode($result);
?>