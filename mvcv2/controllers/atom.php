<?php


include_once '../models/atom.php';
include_once '../models/task.php';
include_once '../models/teacher.php';
if (isset($_GET['id'])) {
    
    $atom = new Atom($_GET['id']);
    $alerts = $atom->getAlerts();
    $teacherName = Teacher::getName($_GET['id']);
    header("Content-Type: application/atom+xml; charset=ISO-8859-1");
    include '../views/atom.php';
}