<?php
include_once '../models/index.php';

include_once '../active-session.php';

session_start();
if (isset($_SESSION['mail']) && isset($_SESSION['token']) && isset($_SESSION['type']))
activeSessionLogin($_SESSION['mail'],$_SESSION['token'],$_SESSION['type']);


$index = new Index();
$projects=$index->displayProjects();
if(isset($_POST['search']))
$search=$index->Search($_POST['search']);



include_once '../views/index.php';
?>