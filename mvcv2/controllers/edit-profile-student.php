<?php
include_once '../active-session.php';


function editProfile()
{
    require '../models/login.php';

    $fields = array();
    $values = array();
    $types = '';
    session_start();
  
    activeSession($_SESSION['mail'],$_SESSION['token'],$_SESSION['type']);

    if (isset($_POST['name']) && strcmp($_POST['name'], '')) {
        array_push($fields, "first_name");
        array_push($values, $_POST['name']);
        $types = $types . 's';
    }

    if (isset($_POST['townborn']) && strcmp($_POST['townborn'], '')) {
        array_push($fields, "last_name");
        array_push($values, $_POST['townborn']);
        $types = $types . 's';
    }

    if (isset($_POST['birthday']) && strcmp($_POST['birthday'], '')) {
        array_push($fields, "birthday");
        array_push($values, $_POST['birthday']);
        $types = $types . 's';
    }

    if (isset($_POST['bio']) && strcmp($_POST['bio'], '')) {
        array_push($fields, "bio");
        array_push($values, $_POST['bio']);
        $types = $types . 's';
    }

    if (isset($_POST['new_password1']) && strcmp($_POST['new_password1'], '')) {
        if (isset($_POST['new_password2']) && strcmp($_POST['new_password2'], '')) {
            if (strcmp($_POST['new_password1'], $_POST['new_password2'])) return "password doesn't match";
            if (isset($_POST['old_password']) && strcmp($_POST['old_password'], '')) {
                $login = new Login($_SESSION["mail"], $_POST['old_password'], $_SESSION["type"]);
                $status = password_verify($_POST["old_password"], $login->getHashedPassword()) ? 1 : 0;
                if ($status == 0) return "Invalid password";
                array_push($fields, "password");
                array_push($values, password_hash($_POST['new_password1'], PASSWORD_DEFAULT));
                $types = $types . 's';
            } else return "Old password must be inserted";
        } else return "Both new password fields should be specified";
    }

    array_push($values, $_SESSION['mail']);
    $types = $types . 's';

    require '../models/edit-profile.php';

    updateProfile($types, $fields, $values, $_SESSION["type"]. 's');
    return "Data successfully changed";
}

$message = editProfile();
include '../views/edit-profile-student.php';
