<?php
include_once '../active-session.php';
session_start();

include '../models/conversation.php';
if (isset($_POST['email']) && isset($_POST['message'])) {
    $myId = getField("id", "email", $_SESSION['mail'], $_SESSION['type'] . 's');
    if ($_SESSION['type'] === 'teacher') {
        $toId = getField("id", "email", $_POST['email'], 'students');
        sendMessage($toId, $myId, 'teacher', $_POST['message']);
    } else {
        $toId = getField("id", "email", $_POST['email'], 'teachers');
       // echo $toId;
        sendMessage($myId, $toId, 'student', $_POST['message']);
        
    }
    header('Content-type:application/json;charset=utf-8');
    $asd = array('msg' => 'Success');
    echo json_encode($asd);
} else {
    $filecontent = json_decode(file_get_contents("php://input"));
    $cv = json_decode($filecontent, TRUE);
    
    $myId = getField("id", "email", $_SESSION['mail'], $_SESSION['type'] . 's');
    if ($_SESSION['type'] == 'teacher') {
        $toId = getField("id", "email", $cv['email'], 'students');
        sendMessage($toId, $myId, 'teacher', $cv['message']);
    } else {
        $toId = getField("id", "email", $cv['email'], "teachers");
        sendMessage($myId, $toId, 'student', $cv['message']);
        
    }

    header('Content-type:application/json;charset=utf-8');
    $asd = array('msg' => "Success");
    echo json_encode($asd);
}
?>