<?php
include_once '../active-session.php';
session_start();
activeSessionforStudent($_SESSION['type']);
activeSession($_SESSION['mail'],$_SESSION['token'],$_SESSION['type']);

include_once '../models/project.php';

$theme = '';
if (isset($_GET['theme'])) {
    $theme = $_GET['theme'];
}
$projects = Project::getAllProposals($theme);

include_once '../views/all-proposals.php';
?>