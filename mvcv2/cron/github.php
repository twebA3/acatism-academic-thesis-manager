<?php
chdir('D:\\xampp\\htdocs\\acatism-academic-thesis-manager\\mvcv2\\cron\\');

require_once '../models/github.php';
require_once '../models/task.php';
require_once '../models/project.php';

$tasks = Task::getGithubTasks(2);
print_r($tasks);
$projects = Project::getAllInProgressWithGithub();
foreach($projects as $project)
{
    $git_info = Github::urlToUserAndRepo($project['github']);
    echo $git_info['username'].' repo: '.$git_info['repo']."\n";
    Github::checkForTasks($git_info['username'], $git_info['repo'], $project['id']);
}