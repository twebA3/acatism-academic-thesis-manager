<?php
chdir('D:\\xampp\\htdocs\\acatism-academic-thesis-manager\\mvcv2\\cron\\');

include_once '../models/task.php';
include_once '../models/project.php';

$projects = Project::getAllInProgress();
foreach($projects as $project)
{
    echo $project['id']."\n";
    Task::sendAlertsOverdue($project['id']);
}
