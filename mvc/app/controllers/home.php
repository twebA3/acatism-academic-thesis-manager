<?php
    class Home extends Controller 
    {
        protected $user;
        public function __construct()
        {
            $this->user = $this->model('User');
        }
        public function index($name='')
        {
            $users = $this->user->getUsers();
            $this->view('home/index', ['name' => $users[0].name]);
        }

        public function create($username, $email)
        {
            User::create([
                'username' => $username,
                'email' => $email
            ]);
        }
    }
?>