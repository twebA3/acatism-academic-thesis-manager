<?php

class Atom extends Controller 
{
    protected $alert;
    public function __construct()
    {
        $this->alert = $this->model('Alert');
    }
    public function feed($id_project)
    {
   
        
        $this->view('atom/index');
      //  Project::all();
        $proj = $this->model('Project');
        $p = $proj->where('id', $id_project)->firstOrFail()->id_teacher;
        echo $p;
        $alerts = Alert::getAlertsByProject($id_project);
        foreach($alerts as $a)
        {
            echo $a->message.'<br>';
        }
    }
}