<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
class Project extends Eloquent
{
    public $name;
    protected $fillable = ['id_teacher', 'id_student', 'name', 'description', 'type'];
}