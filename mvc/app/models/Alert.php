<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
class Alert extends Eloquent
{
    public $name;

    protected $fillable = ['id_project', 'date', 'message'];

    function getAlertsByProject($id_project, $limit=100)
    {
        return self::where('id_project', $id_project)
               ->take($limit)
               ->get();
    }
}