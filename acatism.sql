-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2019 at 06:44 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acatism`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE `alerts` (
  `id` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alerts`
--

INSERT INTO `alerts` (`id`, `id_project`, `message`, `created_at`, `updated_at`) VALUES
(14, 2, 'fesfsefse', '2019-06-08 13:43:06', '0000-00-00 00:00:00'),
(16, 2, 'this is a test', '2019-06-08 12:44:08', '2019-06-08 12:44:08'),
(17, 2, 'this is just a test message', '2019-06-09 19:25:57', '2019-06-09 19:25:57'),
(18, 2, 'test message', '2019-06-11 20:38:04', '2019-06-11 20:38:04'),
(19, 2, 'test message', '2019-06-11 20:39:54', '2019-06-11 20:39:54'),
(20, 2, 'efseesfse', '2019-06-11 20:54:47', '2019-06-11 20:54:47'),
(21, 2, 'testttttttt', '2019-06-11 20:59:27', '2019-06-11 20:59:27'),
(22, 2, 'testttttttt', '2019-06-11 20:59:30', '2019-06-11 20:59:30'),
(23, 2, 'loooool', '2019-06-11 21:02:54', '2019-06-11 21:02:54'),
(24, 2, 'New Task added to your project, description:', '2019-06-11 21:17:10', '2019-06-11 21:17:10'),
(25, 2, 'New Task added to your project, description:', '2019-06-11 21:17:14', '2019-06-11 21:17:14'),
(26, 2, 'New Task added to your project, description:', '2019-06-11 21:17:16', '2019-06-11 21:17:16'),
(27, 2, 'New Task added to your project, description:', '2019-06-11 21:42:27', '2019-06-11 21:42:27'),
(28, 2, 'New Task added to your project, description:', '2019-06-11 21:42:27', '2019-06-11 21:42:27'),
(29, 2, 'New Task added to your project, description:', '2019-06-11 21:42:32', '2019-06-11 21:42:32'),
(30, 2, 'New Task added to your project, description:', '2019-06-12 07:03:47', '2019-06-12 07:03:47'),
(31, 2, 'New Task added to your project, description:', '2019-06-12 07:05:26', '2019-06-12 07:05:26'),
(32, 2, 'New Task added to your project, description:', '2019-06-12 07:07:51', '2019-06-12 07:07:51'),
(33, 2, 'New Task added to your project, description:', '2019-06-12 08:17:31', '2019-06-12 08:17:31'),
(34, 2, 'New Task added to your project, description:', '2019-06-12 08:18:05', '2019-06-12 08:18:05'),
(35, 2, 'New Task added to your project, description:', '2019-06-12 08:26:02', '2019-06-12 08:26:02'),
(36, 2, 'New Task added to your project, description:', '2019-06-12 08:26:59', '2019-06-12 08:26:59'),
(37, 2, 'New Task added to your project, description:', '2019-06-12 09:02:24', '2019-06-12 09:02:24'),
(38, 2, 'New Task added to your project, description:', '2019-06-12 09:39:58', '2019-06-12 09:39:58'),
(39, 2, 'New Task added to your project, description:', '2019-06-12 09:40:32', '2019-06-12 09:40:32'),
(40, 2, 'New Task added to your project, description:', '2019-06-12 09:56:06', '2019-06-12 09:56:06'),
(41, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 09:58:10', '2019-06-12 09:58:10'),
(42, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 09:58:12', '2019-06-12 09:58:12'),
(43, 2, 'New Task added to your project, description:', '2019-06-12 09:58:14', '2019-06-12 09:58:14'),
(44, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 10:25:50', '2019-06-12 10:25:50'),
(45, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 10:26:01', '2019-06-12 10:26:01'),
(46, 2, 'New Task added to your project, description:', '2019-06-12 10:26:04', '2019-06-12 10:26:04'),
(47, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 10:42:24', '2019-06-12 10:42:24'),
(48, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 10:42:28', '2019-06-12 10:42:28'),
(49, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 10:54:09', '2019-06-12 10:54:09'),
(50, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 10:54:12', '2019-06-12 10:54:12'),
(51, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 11:04:43', '2019-06-12 11:04:43'),
(52, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 11:04:49', '2019-06-12 11:04:49'),
(53, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 11:11:01', '2019-06-12 11:11:01'),
(54, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 11:11:04', '2019-06-12 11:11:04'),
(55, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 11:36:53', '2019-06-12 11:36:53'),
(56, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 11:36:59', '2019-06-12 11:36:59'),
(57, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 14:21:49', '2019-06-12 14:21:49'),
(58, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 14:21:52', '2019-06-12 14:21:52'),
(59, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 14:22:05', '2019-06-12 14:22:05'),
(60, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 14:22:07', '2019-06-12 14:22:07'),
(61, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 14:23:28', '2019-06-12 14:23:28'),
(62, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 14:23:30', '2019-06-12 14:23:30'),
(63, 2, 'You have an overdue task: \'yrrrgr\' deadline: 2019-06-10', '2019-06-12 14:25:45', '2019-06-12 14:25:45'),
(64, 2, 'You have an overdue task: \'etetete\' deadline: 2019-06-11', '2019-06-12 14:25:48', '2019-06-12 14:25:48');

-- --------------------------------------------------------

--
-- Table structure for table `helps`
--

CREATE TABLE `helps` (
  `id` int(11) NOT NULL,
  `id_professor` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `id_teacher` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `professors_topics_relation`
--

CREATE TABLE `professors_topics_relation` (
  `id` int(11) NOT NULL,
  `id_professor` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `id_teacher` int(11) NOT NULL,
  `id_student` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  `year` int(5) NOT NULL,
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `github` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `id_teacher`, `id_student`, `name`, `description`, `type`, `year`, `done`, `github`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 'ef', 'fsefes', 'esfesfes', 0, 0, 'https://github.com/Mike97M/A3-api-gateway', '2019-06-12 15:49:20', '0000-00-00 00:00:00'),
(3, 2, 1, 'ssss', 'wwwwww', 'master', 2019, 0, 'https://github.com/Mike97M/test-acatism/', '2019-06-12 16:20:52', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `projects_topics_relation`
--

CREATE TABLE `projects_topics_relation` (
  `id` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `password` varchar(255) NOT NULL,
  `SessionID` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `email`, `birthday`, `password`, `SessionID`, `created_at`, `updated_at`) VALUES
(1, 'jhon', 'doe', 'jhon.doe@test.com', '0000-00-00', '', '', '2019-06-08 13:10:12', '2019-06-08 13:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deadline` date NOT NULL,
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `github` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `id_project`, `description`, `deadline`, `done`, `github`, `created_at`, `updated_at`) VALUES
(1, 3, 'etetete', '2019-06-11', 1, 1, '0000-00-00 00:00:00', '2019-06-12 16:25:34'),
(2, 2, 'yrrrgr', '2019-06-10', 0, 0, '2019-06-12 08:10:08', '2019-06-12 08:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `SessionID` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `first_name`, `last_name`, `email`, `password`, `SessionID`, `birthday`, `created_at`, `updated_at`) VALUES
(2, 'efs', 'fes', 'fes', '', '', '0000-00-00', '2019-06-08 13:42:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tokensforregister`
--

CREATE TABLE `tokensforregister` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `helps`
--
ALTER TABLE `helps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_professor` (`id_professor`),
  ADD KEY `id_student` (`id_student`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_teacher` (`id_teacher`,`id_student`),
  ADD KEY `id_student` (`id_student`);

--
-- Indexes for table `professors_topics_relation`
--
ALTER TABLE `professors_topics_relation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_professor` (`id_professor`),
  ADD KEY `id_topic` (`id_topic`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idt` (`id_teacher`),
  ADD KEY `ids` (`id_student`);

--
-- Indexes for table `projects_topics_relation`
--
ALTER TABLE `projects_topics_relation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_project` (`id_project`),
  ADD KEY `id_topic` (`id_topic`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_projid` (`id_project`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tokensforregister`
--
ALTER TABLE `tokensforregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `helps`
--
ALTER TABLE `helps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `professors_topics_relation`
--
ALTER TABLE `professors_topics_relation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projects_topics_relation`
--
ALTER TABLE `projects_topics_relation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tokensforregister`
--
ALTER TABLE `tokensforregister`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `alerts`
--
ALTER TABLE `alerts`
  ADD CONSTRAINT `id_project` FOREIGN KEY (`id_project`) REFERENCES `projects` (`id`);

--
-- Constraints for table `helps`
--
ALTER TABLE `helps`
  ADD CONSTRAINT `helps_ibfk_1` FOREIGN KEY (`id_student`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `helps_ibfk_2` FOREIGN KEY (`id_professor`) REFERENCES `teachers` (`id`);

--
-- Constraints for table `professors_topics_relation`
--
ALTER TABLE `professors_topics_relation`
  ADD CONSTRAINT `professors_topics_relation_ibfk_1` FOREIGN KEY (`id_professor`) REFERENCES `teachers` (`id`),
  ADD CONSTRAINT `professors_topics_relation_ibfk_2` FOREIGN KEY (`id_topic`) REFERENCES `topics` (`id`);

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `ids` FOREIGN KEY (`id_student`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `idt` FOREIGN KEY (`id_teacher`) REFERENCES `teachers` (`id`);

--
-- Constraints for table `projects_topics_relation`
--
ALTER TABLE `projects_topics_relation`
  ADD CONSTRAINT `projects_topics_relation_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `projects_topics_relation_ibfk_2` FOREIGN KEY (`id_topic`) REFERENCES `topics` (`id`);

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `task_projid` FOREIGN KEY (`id_project`) REFERENCES `projects` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
