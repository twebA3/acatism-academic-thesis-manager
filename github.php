<?php

function getCommits($username, $repo)
{
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => "https://api.github.com/repos/$username/$repo/commits",
        CURLOPT_USERAGENT => 'Codular Sample cURL Request',
        CURLOPT_FOLLOWLOCATION => true
    ]);

    $resp = curl_exec($curl);
    curl_close($curl);
    return json_decode($resp, true);
}

function getTaskId($message)
{
        preg_match('#\[(\d+?)\]#', $message, $matches);        
        return isset($matches[1]) ? $matches[1] : 0;
    
}


$response = getCommits("Mike97M", "test-acatism");
foreach($response as $resp)
{
    $commit = $resp['commit'];
    echo '<br><br>Date: '.$commit['author']['date'];
    echo '<br>Message: '.$commit['message'];
    echo '<br>Task id:'.getTaskId($resp['commit']['message']);
}
?>